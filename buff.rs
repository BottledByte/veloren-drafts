// This is my draft proposal for Veloren's Buff system
// It is a draft pseudo-implementation, so do not expect miracles
// and also note that it might change entirely.
//
// There are two problems so far with the design that needs to be
// discussed/reviewed. Also some performance-wise and codebase-wise review
// should be done eventually, but that is not the most important thing ATM.
//
//
// First (more important one) problem is the "ID-Behaviour incoherence" (my
// term). To clarify, Veloren tends to lack IDs in game logic. Items lack real
// IDs (they have "variant IDs" though), abilities too. That is a fact. It
// allows easier procedural generation/modification of the content (which is not
// done ATM), but it makes behaviour "assumptions" more difficult. The ability
// system is a nice example. There is no way to differ a sword triple strike
// from an axe triple strike without knowing the source Item, as they are the
// same ability enum. Put simply, the ability "ID" is `TripleStrike`, but that
// is all the information you can get from an ability.
//
// This Buff design also allows different behaviour for an
// ID, but IDs are explicit, making it *possible* to enforce the ID=Behaviour
// relationship, but the system does not enforce this by itself. If it would be
// desired, other part of code has to do this, however. Example is
// regeneration. There could be two regens at the same time, each made with
// `RepeatedHealthChange` behaviour, but with different strengths/time. But in
// the current system, they could be both treated as a `Regeneration` or as a
// `RegenWeak` and a `RegenStrong`, it only depends how the IDs would be used.
// There is also a CategoryId, to make things more generic, like having
// positive/negative or natural/magical buffs.
//
// The system could be implemented entirely by using IDs, by mapping an ID to
// actual buff data, not the other way around, i.e. the `BuffId` would point
// to data/behaviour.Something like BuffId::data() (more global state) or
// BuffDataStorage::get(BuffId) (more local state).
// It comes with it's own set of pros and cons. Reference implementation of
// this approach is `comp::Body` or `BlockKind`.
//
//
// The other problem is the buff stacking. Stacking is more problematic in an
// "ID-Behaviour incoherent" system, like in the current item system, where
// memory->persistence direct mapping have proven that the stack of apples can
// have different values than the newly added apple to the stack, changing the
// values somewhat unexpectedly. In this case it was that "old apples" were
// having nerfed stats, but when a stack of them was already in an inventory,
// the new apple "lost it's stats" and was added to the stack with old stats.
// To circumvent this problem, player had to first empty their inventory of
// "old apples", pick a fresh one and then pickup the "old apples" again.
// Suddenly, "old apples" changed their stats due to stacking!
//
// I am not saying that similar case would have to happen to the Buffs, but in
// a "strict ID system", this would never happen. Yet, at the same time, the
// system could loose some flexibility, like unique formulas for buff stacking.
//
use serde::{Deserialize, Serialize};
use specs::Component;
use specs_idvs::IdvStorage;
use std::time::Duration;

/// De/buff ID.
/// ID can be independant of an actual type/config of a `BuffData`.
/// Therefore, information provided by `BuffId` can be incomplete/incorrect.
///
/// For example, there could be two regeneration buffs, each with
/// different strength, but they could use the same `BuffId`,
/// making it harder to recognize which is which.
///
/// Also, this should be dehardcoded eventually.
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum BuffId {
    /// Restores health/time for some period
    Regeneration,
    /// Restores health/time for some period, but faster
    RegenerationStrong,
    /// Lowers health/time for some period
    Poison,
    /// Increases maximum health until time passes
    Blessed,
    /// Lowers maximum health indefitely until uncursed
    Cursed,
}

/// De/buff category ID.
/// Similar to `BuffId`, but to mark a category (for more generic usage, like
/// positive/negative buffs).
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum BuffCategoryId {
    Natural,
    Magical,
    Divine,
    Negative,
    Positive,
}

/// Data indicating and configuring behaviour of a de/buff.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum BuffData {
    RepeatedHealthChange { amount: u32, repeat_time: Duration },
    MaximumHealthChange { amount: u32 },
}

/// Actual de/buff.
/// Buff can timeout after some time if `time` is Some. If `time` is None,
/// Buff will last indefinitely, until removed manually (by some action, like
/// uncursing). The `time` field might be moved into the `Buffs` component
/// (so that `Buff` does not own this information).
///
/// Buff has an id and data, which can be independent on each other.
/// This makes it hard to create buff stacking "helpers", as the system
/// does not assume that the same id is always the same behaviour (data).
/// Therefore id=behaviour relationship has to be enforced elsewhere (if
/// desired).
///
/// To provide more classification info when needed,
/// buff can be in one or more buff category.
///
/// `data` is separate, to make this system more flexible
/// (at the cost of the fact that id=behaviour relationship might not apply).
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Buff {
    pub id: BuffId,
    pub cat_ids: Vec<BuffCategoryId>,
    pub time: Option<Duration>,
    pub data: BuffData,
}

/// Information about whether buff addition or removal was requested.
/// This to implement "on_add" and "on_remove" hooks for constant buffs.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum BuffChange {
    /// Adds this buff.
    Add(Buff),
    /// Removes all buffs with this ID.
    /// TODO: Better removal, allowing to specify which ability to remove
    /// directly.
    Remove(BuffId),
}

/// Component holding all de/buffs that gets resolved each tick.
/// On each tick, remaining time of buffs get lowered and
/// buff effect of each buff is applied or not, depending on the `BuffData`
/// (specs system will decide based on `BuffData`, to simplify implementation).
/// TODO: Something like `once` flag for `Buff` to remove the dependence on
/// `BuffData` enum?
///
/// In case of one-time buffs, buff effects will be applied on addition
/// and undone on removal of the buff (by the specs system).
/// Example could be decreasing max health, which, if repeated each tick,
/// would be probably an undesired effect).
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Buffs {
    /// Active de/buffs.
    pub buffs: Vec<Buff>,
    /// Request to add/remove a buff.
    /// Used for reacting on buff changes by the ECS system.
    /// TODO: Can be `EventBus<T>` used instead of this?
    pub changes: Vec<BuffChange>,
    /// Last time since any buff change to limit syncing.
    pub last_change: f64,
}

impl Buffs {
    /// Adds a request for adding given `buff`.
    pub fn add_buff(&mut self, buff: Buff) {
        let change = BuffChange::Add(buff);
        self.changes.push(change);
        self.last_change = 0.0;
    }

    /// Adds a request for removal of all buffs with given Id.
    /// TODO: Better removal, allowing to specify which ability to remove
    /// directly.
    pub fn remove_buff_by_id(&mut self, id: BuffId) {
        let change = BuffChange::Remove(id);
        self.changes.push(change);
        self.last_change = 0.0;
    }

    /// This is a primitive check if a specific buff is present.
    /// (for purposes like blocking usage of abilities or something like this).
    pub fn has_buff_id(&self, id: &BuffId) -> bool { self.buffs.iter().any(|buff| buff.id == *id) }
}

impl Component for Buffs {
    type Storage = IdvStorage<Self>;
}
