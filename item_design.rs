/// Id of a material.
/// Essentially material list, could be deharcoded
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum MaterialId {
    Stone,
    Wood,
    Bread,
}

impl MaterialId {
    /// Returns actual properties of a material
    pub fn base_properties(&self) -> MaterialProperties {
        match self {
            MaterialId::Stone => MaterialProperties {
                weight: 30,
                hardness: 17,
                flammable: 0,
            },
            MaterialId::Wood => MaterialProperties {
                weight: 15,
                hardness: 6,
                flammable: 50,
            },
            MaterialId::Bread => MaterialProperties {
                weight: 12,
                hardness: 1,
                flammable: 1,
            },
        }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
/// Properties of a material per some unit.
/// These could be used in physics and combat.
pub struct MaterialProperties {
    weight: i32,
    hardness: u32,
    flammable: u32,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
/// Amount of material on an item.
/// Rather than storing actual material, Id is used to make it more flexible
pub struct ItemMaterial {
    amount: u32,
    kind: MaterialId,
}

/// Id for an item modifier
/// List, could be dehardcoded
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum ItemModifierId {
    Broken,
    Glowing,
    Cursed,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
/// Actual item modifier.
/// Modifiers should change "base item" and should be undoable at any time. 
pub struct ItemModifier {
    id: ItemModifierId,
    data: String, // some behaviour, reference to behaviour, or config...
}

/// Some body slot.
/// Effectively list, it could be dehardcoded
/// allowing creation of unique physiology (like 4-handed giants, ghosts, etc)
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum BodySlot {
    Chest,
    Head,
    Back,
}

/// Id of an ability.
/// Could be dehardcoded.
/// I am introducing this structure in my MR.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum AbilityId {
    SwordCut,
    AxeSwing,
    Heal,
}

/// Actual ability.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Ability {
    id: AbilityId,
    data: super::super::CharacterAbility,
}

/// Id of some character de/buff.
/// Could be dehardcoded. Probably will be used to pair assets.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum BuffId {
    Regenerating,
    Blind,
    Hastened,
}

/// Actual de/buff.
/// Buff could be implemented as data (like are abilities ATM)
/// or script references (at very distant future).
/// This wrapper does not make any assumptions about implementation.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Buff {
    id: BuffId,
    data: String, // some behaviour, reference to behaviour, or config...
}

/// Enum for flagging usage cases for items.
/// Like can be used in crafting or quest tracking
/// TODO: Is this enum needed?
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum ItemFunctionality {
    Quest,
    CraftingTool,
    Ingredient,
}

/// Something that item can do or how it behaves.
/// This is the spirit of the entire proposal.
/// Instead on relying on data in "kinds" and pairing behaviour to certain kind,
/// which effectively allows only single purpose/use of an item
/// this makes "Things can be used in different ways" concept possible.
///
/// I am not sure if this particular proposal is exactly the most moddable way,
/// as I do not see how an external script could add new types of properties,
/// but this property design opens up so many possibilities that
/// it should not ever be required.
///
/// To argument this system, I will provide an example of
/// how I think about it:
/// Consider a primitive, common item, like apple,
/// and then consider all actions you can do with an apple
/// - You can eat it                    `Consumable`
/// - You can hold it                   `Wieldable`
/// - You can throw it                  `Throwable`
/// - You can use it to lure animals    `GrantsAbilities`
/// - You can crush it                  `Damageable`
/// - You can make an apple pie with it `Functional`
///
/// As you can see, there is quite a lot of things you can do with an apple.
/// I admit that some of them are somewhat questionable, but even such simple
/// thing has more uses than as a simple `Ingredient` or `Consumable` (`ItemKind` equivalents).
/// 
/// Currently, Item has only one ItemKind, which makes it far more difficult
/// to use it in different ways than it's ItemKind allows.
/// This property system is much more flexible compared to `ItemKind`,
/// but it might lead to new kinds of bugs, mostly Material/ItemProperty related.
/// (Something like consumable stones, but there could be beings capable
/// of that, so... *it's not a bug, it's a feature*)
///
/// I think that because Veloren uses ECS internally, such item design is more natural.
/// For example, all entities can glow, so why should we restrict Items like
/// "Only Lantern can glow."? Magical armor can not produce a little amount of light?
/// Or like "Only Bomb can be thrown". Is it impossible to throw a dagger?
///
/// These things could be solved case-specific, but it might be more
/// interesting and functional to do them in a more generic way (like EC or ECS).
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum ItemProperty {
    /// Item can be weared
    Wearable {
        slot: BodySlot,
    },
    /// Item can be hold in hand/hands
    /// TODO: How to treat wielding?
    Wieldable,
    /// Item can be eaten, providing effects
    Consumable {
        effects: Vec<Effect>,
    },
    /// Item provides some abilities (can include attack)
    GrantsAbilities {
        abilities: Vec<Ability>,
    },
    /// Item provides some abilities (can include attack)
    GrantsPassiveBuffs {
        buffs: Vec<Buff>,
    },
    /// Item glows
    Glowing {
        color: Rgb<u32>,
        strength_thousandths: u32,
        flicker_thousandths: u32,
    },
    /// Item can be thrown
    Throwable {
        aerodynamicity: i32, // And why not? :)
    },
    /// Item protects it's owner
    /// TODO: add something like on_wear/wield???
    Protects {
        stats: armor::Stats,
    },
    /// Item can cause damage
    /// TODO: This is very questionable, as damage is done through abilities really.
    /// TODO: and passive effects should be de/buffs
    Damages {
        stats: tool::Stats,
    },
    /// Item can be damaged
    Damageable {
        hitpoints: u32,
    },
    /// Item can have some specific functionality, like a quest item,
    /// or crafting ingredient or tool
    Functional {
        functions: Vec<ItemFunctionality>,
    },
}

/// Actual Item.
///
/// `stack` is a property that every Item have (on or off), but it might be an ItemProperty.
///
/// Also, both `name` and `description` should not be treated as primitive Strings (they are ATM),
/// but rather as translation keys. They could be strings in a special form,
/// like "#IT_RUSTY_SWORD" and "#IT_RUSTY_SWORD_DESC",
/// so that client could pair those with it's translation table.
///
/// I think that `Vec`s are passable solution, even through the iteration
/// required to find a property/modifier/material, as these things should not be queried too often.
/// Other ways I thought of were fixed-length arrays with predefined keys and `HashMap`s.
///
/// Note that **items still do not have Ids**.
/// It might be desirable, but before that, a working id system should exist within Veloren
/// (either string intering or even numeric-to-string ids).
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Item {
    name: String,
    description: String,
    pub materials: Vec<ItemMaterial>,
    pub properties: Vec<ItemProperty>,
    pub modifiers: Vec<ItemModifier>,
    pub stack: Option<u32>
}
